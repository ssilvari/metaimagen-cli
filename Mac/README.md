# Requirements
For installing and running MetaImaGen in Mac you just need:

*  System: macOS El Capitan 10.11 (minimum)
*  RAM: 8GB minimum
*  Storage: 20 GB free minimum
*  CPU: 4 cores (any type)

## How do I execute it?
Go to the download folder and run:
```bash
./metaimagen.sh [FreeSurfer_FOLDER] [GROUPFILE.CSV] [OUTPUT_FOLDER] [ID]
```
