## <span style="color:blue">Still under development!</span>

# Requirements
For installing and running MetaImaGen in Windos you need:

*  System: Windows 7/8/8.1/10/Server 2016 (64-bit)
*  RAM: 12GB minimum
*  Storage: 30 GB free minimum
*  CPU: 4 cores (any type)

## How do I execute it?
Go to the download folder and run:
```bash
metaimagen.exe [FreeSurfer_FOLDER] [GROUPFILE.CSV] [OUTPUT_FOLDER] [ID]
```
