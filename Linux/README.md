# MetaImaGen for Linux

## Requirements
For installing and running MetaImaGen in Linux you need:

*  A 64-bit Linux distribution (CentOS, Fedora, Debian, Ubuntu, etc.)
*  RAM: 8GB minimum
*  Storage: 20 GB free minimum
*  CPU: 4 cores (any type)

## How do I execute it?
Go to the download folder and run:
```bash
./metaimagen.sh [FreeSurfer_FOLDER] [GROUPFILE.CSV] [OUTPUT_FOLDER] [ID]
```
